#!/usr/bin/python3

class FileWriter:
    '''This class the emails in a txt file'''

    mailDict = {}
    @staticmethod
    def textWriter(emails):
        '''Method to write every email that the scrapper gets'''
        
        f = open("Emails.txt", "a")
        for email in emails:
            busq = FileWriter.mailDict.get(email)
            if busq != 1:
                FileWriter.mailDict[email] = 1
                f.write(email+"\n")
        f.close()