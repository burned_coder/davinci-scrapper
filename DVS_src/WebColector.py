#!/usr/bin/python3

class WebColector:  
    '''The class work similar to a Data Base, saves every link found by regular expressions'''    

    _col=[]
    _todo={}
    
    @staticmethod
    def addCol(item):
        '''Save the links, in a dictionary and in a list. The dictionary saves all and never delete a entry. With get() we search a entry, if it is in the dictionary, then the link will be ignored'''

        searchValue=WebColector._todo.get(item)
        if searchValue!=1:
            WebColector._col.append(item)
            WebColector._todo[item]=1

    @staticmethod
    def getColElements():
        '''Method that return the list'''

        return WebColector._col

    @staticmethod
    def __str__():
        '''This method print every element of the list'''

        for elem in WebColector._col:
            print(elem)

    @staticmethod
    def firstElem():
        '''In this case, we return the first element of list'''

        #WebColector.name=self.col[0]
        return WebColector._col[0]

    @staticmethod    
    def deleteFirstElem(elem):
        '''Here we delete a element(link) once it was visited'''
        
        WebColector._col.remove(elem)