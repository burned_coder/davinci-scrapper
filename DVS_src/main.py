#!/usr/bin/python3

import RegularExpresion
import WebReader
import WebColector
from rich.console import Console

def main():
    '''Main Method'''
    
    console = Console()
    #[bold yellow]
    console.print("[bold blue] ____    __  _  _  ____  _  _  ___  ____    ___   ___  ____    __    ____  [/bold blue]")
    console.print("[bold blue](  _ \  /__\( \/ )(_  _)( \( )/ __)(_  _)  / __) / __)(  _ \  /__\  (  _ \ [/bold blue]")
    console.print("[bold blue] )(_) )/(__)\\\  /  _)(_  )  (( (__  _)(_   \__ \( (__  )   / /(__)\  )___/  [/bold blue]")
    console.print("[bold blue](____/(__)(__)\/  (____)(_)\_)\___)(____)  (___/ \___)(_)\_)(__)(__)(__) [/bold blue]")    
    print()
    print()
    dominio = console.input("[bold red]Escriba un primer dominio: [/bold red]")
    print()
    html=WebReader.WebReader.readWeb(dominio)
    if html!=None:
        RegularExpresion.Scrapper.regularExpres(html)
        RegularExpresion.Scrapper.regularExpresMail(html)
    salir=False

    while salir==False:
        web=WebColector.WebColector.firstElem()
        WebColector.WebColector.deleteFirstElem(web)
        console.print("[bold green]--> "+web+"[/bold green]")
        html=WebReader.WebReader.readWeb(web)
        if html!=None:
            RegularExpresion.Scrapper.regularExpres(html)
            RegularExpresion.Scrapper.regularExpresMail(html)

main()