#!/usr/bin/python3

from logging import exception
from urllib.request import Request, urlopen
from urllib.error import URLError, HTTPError

class WebReader:
    '''Class that contains a method to get the string of a web'''

    @staticmethod
    def readWeb(dom):
        '''This Method uses request to transform a web to a string'''
        
        try:
            req= Request(dom)
            page=urlopen(req)
            html=page.read().decode("latin-1")
            return html
        except HTTPError as he:
            print("")
        except URLError as ue:
            print("")
        except ValueError as ve:
            print("")
        except BaseException as be:
            print("")
        except exception as e:
            print("")
        #RegularExpresion.Scrapper.regularExpres(html)
        #RegularExpresion.Scrapper.regularExpresMail(html)