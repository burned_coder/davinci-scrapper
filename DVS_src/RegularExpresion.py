#!/usr/bin/python3

#from urllib.request import Request, urlopen
import re
import WebColector
import FileWriter

class Scrapper:  
    '''This class use get the data with regular expressions'''
     
    #Metodo para Expresiones Regulares
    @staticmethod
    def regularExpres(web):
        '''Method that get the links of a web with a regular expression'''

        #href=\".+?\.(?:html|org|org\..{2,3}|es|com|net|com\..{2,3}|co\..{2,3}|mx)\"
        #<a href=\".+?\">
        ar=re.findall(r"href=\".+?\"",web,re.IGNORECASE)
        webs=[]
        
        for word in ar:   
            #Sustituimos caracteres indeseados
            pal=re.sub("(?:href=|\")","",word)
            #Añadimos los valores a la lista   
            if pal[0] != "/":        
                WebColector.WebColector.addCol(pal)

        #WebColector.WebColector.__str__()
    @staticmethod
    def regularExpresMail(web):
        '''The Second method habe a regular expression that get the emails of a web page'''

        ar=re.findall(r"\b(?:\w|\.|\-|\_|@)+?@\w+?\.\w{2,3}\b",web,re.IGNORECASE)
        if len(ar)>0:
            FileWriter.FileWriter.textWriter(ar)


    #Metodo para leer una web
    #@staticmethod
    #def readWeb(dom):
        #req= Request(dom)
        #page=urlopen(req)
        #html=page.read().decode("latin-1")
        #Scrapper.regularExpres(html)
        #Scrapper.regularExpresMail(html)

    #For Emails
    # \b(\w|[[:punct:]])+@.*\.(?:com.\w{2}|es|fr|uk|org|net|edu|gov|mil|th|be|gr|it|com)\b
    #For Complex Webs
    # Best Result: \"(?:http.{3}|https.{3})?w{3}..+?\"(?:\s|\>
    # \b(?:http.{3}|https.{3})?w{3}..+?\.(?:\w{2,5}).*?\.\w{2,5}\b
    # \b(?:http.{3}|https.{3})?w{3}..+?\.(?:\w{2,5}).*?\.(?:index|html|com|es|fr|ul|it)\b
    # "\b(?:http.{3}|https.{3})?w{3}..+?\.(?:\w{2,5})"
    # For Simple Webs
    #\b(?:http.{3}|https.{3})?w{3}..+?(?:\.|\/)(?:\w{2,5})\b

    


